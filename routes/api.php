<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });





Route::prefix('object')->group(function () {
    Route::post('/', [ApiController::class, 'set_object']);
    Route::get('/test', [ApiController::class, 'test']);
    Route::get('/get_all_records', [ApiController::class, 'all_records']);
    Route::get('/{mykey}', [ApiController::class, 'get_object']);
});

Route::fallback(function () {
    return response()->json(['error' => 'Not Found!'], 404);
});