<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Carbon\Carbon;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for($i=0;$i<100;$i++){
            DB::table('key_value_store')->insert([
                'key'  => (string) Str::uuid(),
                'value' => json_encode([
                    'title'=>$faker->name,
                    'age'=>$faker->numberBetween(25, 50),
                ]),
                'created_at'=>Carbon::now()->timestamp
            ]);
        }
    }
}
