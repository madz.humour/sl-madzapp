<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DeleteColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('key_value_store', function (Blueprint $table) {
            $table->dropColumn(['updated_at', 'deleted_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('key_value_store', function (Blueprint $table) {
            $table->integer('updated_at')->nullable();
            $table->integer('deleted_at')->nullable();
        });
    }
}
