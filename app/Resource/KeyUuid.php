<?php 

namespace App\Resource;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Models\KeyValueStore;
use App\Models\View_KeyStoreLatest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Exception;


class KeyUuid {



	public function generateUniqueKey(Model $model,$field,$count=0){
		$uuid = (string) Str::uuid();
		$record = $model->where($field,$uuid)->first();
		if (is_null($record)){
			return $uuid;
		}
		if($count > 5){
			throw new Exception('Unable to generate Uuid with multiple tries');
		}
	    return $this->generateUniqueKey(new get_class($model),$field,0);
	}

}