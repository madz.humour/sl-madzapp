<?php 

namespace App\Resource;
use Illuminate\Http\Request;
use App\Models\KeyValueStore;
use App\Models\View_KeyStoreLatest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Resource\KeyUuid;


class KeyStoreResource {


	public function get_all_records(Request $req){
		$items_per_pg = config('app.settings.default_pagination');
		$tbl_name_KeyStore = (new KeyValueStore())->getTable();
        $tbl_name_v_KeyStoreLatest = (new View_KeyStoreLatest())->getTable();

        return $records = DB::table($tbl_name_KeyStore) //to only include the current value of each key using mysql view
                    ->rightJoin($tbl_name_v_KeyStoreLatest,'id',$tbl_name_v_KeyStoreLatest.".max_id")
                    ->select($tbl_name_KeyStore.".*")
                    ->paginate($items_per_pg);
	}


	public function get_single_object($mykey,Request $req){
		$timestamp = $req->input('timestamp');
        $query = KeyValueStore::where('key',$mykey)->orderBy('id', 'desc');
        if($timestamp){
            $query->where('created_at',$timestamp);
        }
        return $query->firstOrFail();
	}

	public function set_object(Request $req){
		
		$input = $req->input();
        $mykey = null;
        $value = null;
        $is_new_record =true;
        $vdata = ['value'=>'required'];
        $keyUuid = new KeyUuid;
        $no_change =false;


        foreach($input as $key => $v){
            if($mykey){ //key is found and ignore others
                break;
            }
            if(Str::isUuid($key)){
                $mykey = $key;
            }
            $value = $v;
        }

        $request = new Request([
            'key' => $mykey,
            'value' => $value
        ]);

        $request->validate($vdata);
        $old_record = KeyValueStore::where('key',$mykey)->orderBy('id', 'desc')->first();
        $is_new_record = $old_record ? false : true;
        $data  = [
            'key'=>$mykey,
            'value'=>$value,
            'created_at'=>Carbon::now()->timestamp
        ];
        if($is_new_record ){ 
            $data['key'] = $keyUuid->generateUniqueKey(new KeyValueStore,'key');
        }

        if(!$is_new_record){ //check if the value has been changed
        	$no_change = hash("sha256",$old_record['value']) == hash("sha256",$value);
        }

        $result = $no_change ===false ? KeyValueStore::create($data) : $old_record; //save only when change has happened
        return $result;
	}


}

