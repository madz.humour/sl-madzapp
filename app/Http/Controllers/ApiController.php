<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Resource\KeyStoreResource ;


use Illuminate\Support\Facades\DB;
use App\Models\KeyValueStore;
use App\Models\View_KeyStoreLatest;
use Illuminate\Support\Str;

class ApiController extends Controller
{
    public function all_records(Request $req){
        $resource = new KeyStoreResource;
        $records = $resource->get_all_records($req);
        return response()->json($records);
    }

    public function get_object($mykey,Request $req){
        $resource = new KeyStoreResource;
        $record = $resource->get_single_object($mykey,$req);
        return response()->json($record);
    }

    public function set_object(Request $req){
         $resource = new KeyStoreResource;
         $record = $resource->set_object($req);
         return response()->json($record);
    }
    
}
