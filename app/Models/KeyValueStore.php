<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KeyValueStore extends Model
{
    
    protected $table = 'key_value_store';
    protected $casts = [
        'value' => 'array',
    ];
    protected $fillable = ['value','key','created_at'];
    public $timestamps = false;

}
