<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class View_KeyStoreLatest extends Model
{
    
    protected $table = 'view_keystore_latest';
    protected $casts = [
        'value' => 'array',
    ];
    protected $fillable = [];
    public $timestamps = false;

}
