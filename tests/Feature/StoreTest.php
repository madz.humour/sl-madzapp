<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Support\Facades\DB;


class StoreTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }


    public function test_test_single_404(){
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->get('/api/object/abcd', []);

        $response->assertStatus(404);
    }

    public function test_test_single_existing(){
        $record = DB::table('key_value_store')
                ->inRandomOrder()
                ->first();
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->get('/api/object/'.$record->key, []);
        $response->assertStatus(200);
    }
}
